
public class Proveedor extends Empresa
{
    
    public Proveedor(String nombre){
    
        super(nombre);
        this.cantidad_productos=100;
    }
    
   
    
    public void venta(Empresa empresa,int num_producto){
        this.cantidad_productos-=num_producto;
        empresa.compra(num_producto);
    }
}
