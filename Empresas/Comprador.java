public class Comprador extends Empresa
{

    public String nombre_producto;
   public Comprador(String nombre){
    super(nombre);
    this.cantidad_productos=0;
    this.nombre_producto="";
    }
    
    
    public void setNombreProducto(String nombre){
        this.nombre_producto=nombre;
    }
    
    public void compra(int num_productos){
        this.cantidad_productos+=num_productos;
    }
    
    public void venta(){
     System.out.println("Tengo productos a la venta para usuarios: el producto "+ this.nombre_producto);
    }
    
}
